/*
 * ramdom_array.h
 *
 * This is the implementation for fetching the random array to bubblesort algorithm
 *
 */

#ifndef _ARRAYGENERATOR_H_
#define _ARRAYGENERATOR_H_

#include "bubblesort.h"
#include <cstdlib>

#define R_MAX 200

class arraygenerator
{
  /**
   *
   * When you create an object of this class you need to pass the number of arrays you want to sort and also the length of them.
   *
   *
   * This class has mutators for extracting the data stored by class bubblesort
   *
   * @Author:          Saujan Thapa
   * @Student number:  n9003096
   * @Unit:            CAB301
   * @version:         1.0
   */
 private:
   //stores the number of arrays that needs to be sorted
   int how_many_arrays;
   
   int* length_each_array;
   //pointers of pointers of instance of bubblesort
   bubblesort** bub_sort;
   //sends the n number of random elements
   int* list_random_value(int n);
   //sends the n number of user-input elements
   int* list_userdefined_value(int n);

 public:
   //initializes the data members by creating the array of objects
   arraygenerator(int arrays,int* ind_length);
   //gives the option to select random or manul arrays
   int input_array_option();
   //it creates the bubblesort objects by passing the arrays
   void create_arrays(int option);
   //frees the memory of used by object of bubblesort
   ~arraygenerator();
   //displays the information regarding bubblesort algorithm
   void summary();
};

#endif
