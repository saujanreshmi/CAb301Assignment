
/*
 * bubblesort.h
 *
 * This is the implementation of algorithm given to us for solving regarding Assignment 1.
 * This is a bubble sort algorithm implementation using class.
 *
 */

#ifndef _BUBBLESORT_H_
#define _BUBBLESORT_H_

#include <iostream>


class bubblesort
{
  /**
   *
   * When you create an object of this class you need to pass the array of data you want to be sorted.
   *
   *
   * This class has mutators for extracting the sorted array,time taken to sort the array and size of array.
   *
   * @Author:          Saujan Thapa
   * @Student number:  n9003096
   * @Unit:            CAB301
   * @version:         1.0
   */
private:
 //stores the raw array
 int *r_array;
 //stores the sorted array
 int *s_array;
 //stores the raw array for counting basic opration
 int *temp;
 //store the number of total number of members in array
 int length;
 //stores the total time taken to sort the array
 double time_elapsed;
 //counts the number of basic operation
 long long no_operation;
 //stores true if sorting operation is true and false otherwise
 bool is_correct;
 //this method swaps the two values using pointers
 void swap(int *first,int *second);

public:
  /**  initializes the member datatypes by performing all the sorting operation
    *
    *
    */
  bubblesort(int *A,int size);
  //destroys the space consumed by bubblesort
  ~bubblesort();
  // calculates the time taken when sorting the algorithm
  void calcualte_timespan_inbubble_sort(void);
  // calculates the number of basic operating by bubblesort algorithm
  void calculate_no_opration_inbubble_sort(void);
  //returns the size of the this array;
  int size_of_array();
  // this method extracts the sorted array.
  int* sorted_array();
  // this mehtod extracts the original array.
  int* raw_array();
  // this method sends the total time taken for sorting the array
  double time_taken();
  // this method sends the total number of basic opration performed by bubblesort algorithm
  long long number_of_operation();
  // this method sends if the algorithm is correct
  bool is_algorithm_correct();

};

#endif
