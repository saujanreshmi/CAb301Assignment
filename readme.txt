##############
This project gives you the facility of checking bubble sort algorithm for sorting series of numbers in non-decreasing order.

This program also gives you the option for manual entry of arrays or random arrays 

#############
How to run??
=> clone the repository and simply run the makefile
=> to run the program type: make runmain
#############
How to clean??
=> type: make clean

