all: main

#class objects
obj/bubblesort.o : include/bubblesort.h src/bubblesort.cpp
	    g++ -ansi -c -o obj/bubblesort.o -I include/ src/bubblesort.cpp

obj/arraygenerator.o : include/arraygenerator.h include/bubblesort.h src/arraygenerator.cpp
	    g++ -ansi -c -o obj/arraygenerator.o -I include/ src/arraygenerator.cpp

#test objects
obj/main.o : include/bubblesort.h include/arraygenerator.h src/main.cpp
	    g++ -ansi -c -o obj/main.o -I include/ src/main.cpp

#test executable
main: obj/bubblesort.o obj/arraygenerator.o obj/main.o
	    g++ -o main obj/bubblesort.o obj/arraygenerator.o obj/main.o

#run tests
runmain : main
	./main

clean:
	rm -f obj/*.o
	rm -f main
