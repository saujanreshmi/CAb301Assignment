 /*
  * main.cpp
  *
  * This is the tester code for bubble sort through randomarray
  *
  * Contains main function
  */

/* HEADER FILES */
//local headerfiles
#include"bubblesort.h"
#include"arraygenerator.h"

//standard headerfiles
#include <iostream>

using namespace std;
//function prototypes
void test_bubble_sort(void);

//main function
int main(int argc, char* argv[])
{
  test_bubble_sort();
  return 0;
}

//this method tests the bubblesort algorithm through randomarray
void test_bubble_sort(void)
{
  int no_list=0;
  int* length_each_array;
  cout <<endl<<"===================================WELCOME TO BUBBLESORT ALGORITHM TESTER========================================"<<endl;
  cout << " => Enter the number of arrays you want to test: ";
  cin >> no_list;
  length_each_array = new int[no_list];
  for (int index=0;index<no_list;index++)
  {
    cout << " => Enter the length of array " << index+1 <<" : ";
    cin >> length_each_array[index];
  }
  arraygenerator* ag1 = new arraygenerator(no_list,length_each_array);
  int random_or_manual=ag1->input_array_option();
  ag1->create_arrays(random_or_manual);
  ag1->summary();

  delete ag1;
}
