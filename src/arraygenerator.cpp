 /*
  *
  * ramdomarray.cpp
  *
  * This file has the definition of randomarray.h
  *
  *
  * @Author:          Saujan Thapa
  * @Student number:  n9003096
  * @Unit:            CAB301
  * @version:         1.0
  */
#include "arraygenerator.h"

using namespace std;

//this method sends out the list of random array of provided length
int* arraygenerator :: list_random_value(int n)
{
  int* values=new int[n];
  for (int i=0;i<n;i++)
  {
    values[i]=rand()%R_MAX;
  }
  return values;
}

//this method sends out the list of user-defined array of provided length
int* arraygenerator :: list_userdefined_value(int n)
{
  int* values=new int[n];
  for (int i=0;i<n;i++)
  {
    cin>>values[i];
  }
  return values;
}

//constructor of the arraygenerator class
/**
  * this method also gives the option to select if we want random or manual arrays
  */
arraygenerator :: arraygenerator(int arrays, int* ind_length)
{
  how_many_arrays = arrays;
  //dyanamic initialization of arrays of empty objects.
  bub_sort = new bubblesort*[arrays];
  length_each_array = ind_length;
}

//destructor of the arraygenerator class
arraygenerator :: ~arraygenerator()
{
  for (int i=0;i<how_many_arrays;i++)
  {
    delete bub_sort[i];
  }
  delete bub_sort;
}

//gives the option to select random or manul arrays and feedbacks the option
int arraygenerator :: input_array_option()
{
  int choice=0;
  cout<<"     -------------------"<<endl;
  cout<<"       Select Option    "<<endl;
  cout<<"     -------------------"<<endl;
  cout<<"       1. Random arrays"<<endl;
  cout<<"       2. Manual arrays"<<endl;
  cout<<"     -------------------"<<endl;
  cout<<"       Enter the value(1-2): ";
  do{
    cin>>choice;
    if(choice!=1&&choice!=2)
    {
      cout<<"Invalid input! Please enter again: ";
    }
  }while(choice!=1&&choice!=2);
   return choice;
}

//initializes the created array of objects
void arraygenerator :: create_arrays(int option)
{
  for (int i=0;i<how_many_arrays;i++){
    //choices between random or manual
    switch(option){
      case 1:
        bub_sort[i]= new bubblesort(list_random_value(length_each_array[i]),length_each_array[i]);
        break;
      case 2:
        cout<<"Enter the array "<<i+1<<" of length "<<length_each_array[i]<<" :"<<endl;
        bub_sort[i]= new bubblesort(list_userdefined_value(length_each_array[i]),length_each_array[i]);
        cout<<"\n\n";
        break;
    }
  }
    cout << " Calculating ....."<<endl;
    for (int i=0;i<how_many_arrays;i++){
    bub_sort[i]->calcualte_timespan_inbubble_sort();
    bub_sort[i]->calculate_no_opration_inbubble_sort();
    }
}

//this method feedbacks the summary of the bubblesort algorithm
void arraygenerator:: summary()
{
  //keeps track of basic operation and stores the average
  long long average_basic_opration=0;
  //keeps track of execution time and stores the average
  double average_time_taken=0;
  cout <<"================================================SUMMARY==========================================================="<<endl;
  for (int i=0;i<how_many_arrays;i++)
  {
    cout<<" S.N :                       ";
    cout<<i+1<<endl;
    //.....................................//
    cout<<" Array Length :              ";
    cout<<bub_sort[i]->size_of_array()<<endl;
    //.....................................//
    cout<<" Functional Check :          ";
    if(bub_sort[i]->is_algorithm_correct()){
      cout<<"Algorithm is Correct"<<endl;
    }
    else{
      cout<<"Algorithm is Not Correct"<<endl;
    }
    //.....................................//
    cout<<" Original Array :            ";
    for(int index=0;index<bub_sort[i]->size_of_array();index++)
    {
      cout<<bub_sort[i]->raw_array()[index]<<" ";
    }
    cout<<endl;
    //.....................................//
    cout<<" Sorted Array :              ";
    for(int index=0;index<bub_sort[i]->size_of_array();index++)
    {
      cout<<bub_sort[i]->sorted_array()[index]<<" ";
    }
    cout<<endl;
    cout<<" Time Taken(seconds) :       ";
    cout<<bub_sort[i]->time_taken()<<endl;
    cout<<" Number of Basic Operation : ";
    cout<<bub_sort[i]->number_of_operation()<<endl;
    cout <<"------------------------------------------------------------------------------------------------------------------"<<endl;
    average_basic_opration+=bub_sort[i]->number_of_operation();
    average_time_taken+=bub_sort[i]->time_taken();
  }
  cout<<"Average number of basic operation: "<<average_basic_opration/how_many_arrays<<endl;
  cout<<"Average exection time: "<<average_time_taken/how_many_arrays<<endl;
}
